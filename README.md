# Exercices liés aux diagrammes de séquence en UML

## Nono le petit robot

```mermaid
classDiagram

direction LR

Robot*--> Bras

Bras *--> Pince

class Robot {

+chercherPiece()

}

class Pince{

+saisir()

+lacher()

}

class Bras{

+deployer()

+retracter()

+saisirPiece()

+lacherPiece()

}
```

La situation est celle d'un petit bras articulé capable de déployer ou de rétracter son, bras et d'ouvrir ou de fermer une pince pour aller chercher des objects lorsque l'ordre lui est donné. 

Les trois participants à la collaboration  étudiée sont le robot, le bras et la pince.

Proposer les diagrammes de séquences pour les interactions suivantes : 
- L'ordre est envoyé d'aller chercher un objet
- Le robot déploie son bras
- Le robot saisit l'objet
- Le robot rétracte son bras 
- Le robot relache l'objet

## Une manche au sein d'une partie de poker

Il s'agit de modéliser un nouveau tour au sein d'une manche  de poker. Voici le déroulé attendu : 
- L'interface demande si de nouveaux joueurs souhaitent participer à la manche. 
- Si tel est le cas les utilisateurs sont ajoutés à la manche. 
- Toutes les mains des joueurs sont vidées et remises dans le deck. 
- Le deck est mélangé.
- Le premier joueur doit réaliser une mise de départ
- chaque joueur recoit deux cartes (une à la fois)
- Si le joueur a gauche du premier joueur n'a pas assez d'argent il est retiré de la partie
- Si le joueur suivant a assez d'argent il peut miser, augmenter la mise ou quitter la table
- Le cycle continue jusqu'à ce que tous les joueurs actifs aient misé le meme montant.

## Ajout d'un rendez-vous au sein d'un calendrier

Le scenario débute lorsqu'un utilisateur A définit un nouveau rendez-vous au sein d'une interface graphique avec un autre utilisateur B.

L'interface part du jour actuel et affiche une pop-up de date et heure.
L'utilisateur A saisit le nom, le lieu, la date, l'heure de début et l'heure de fin du rendez-vous.
L'interface interdit une incohérence dans les données tel qu'un nom vide ou une durée négative.

Si l'utilisateur B avait déjà un rendez-vous sur cette plage de temps il est demandé à l'utilisateur de saisir une autre plage de temps et l'ajout est refusé.
Si l'utilisateur B avait déjà un rendez-vous avec le même nom et les mêmes dates de début et de fin, il est demandé à l'utilisateur 1 s'il souhaite se rajouter à la liste de partipants de cette réunion.
Si oui, il est alors ajouté aux participants, le cas échéant, il lui est demandé de spécifier de nouvelles dates. 
Le rendez-vous est ajouté au calendrier de l'utilisateur B.
Le rendez-vous est ensuite ajouté au calendrier de l'utilisateur A.
